const border = document.querySelector(".border");
const circle = document.querySelector(".circle");

function up(e) {
    circle.style.left = 400 - e.clientX - 30 + "px";
    circle.style.top = 400 - e.clientY - 30 + "px";
}

border.addEventListener("mousemove", up, false);
circle.addEventListener("mouseenter", function(e){circle.style.backgroundColor = "green"});
circle.addEventListener("mouseout", function(e){circle.style.backgroundColor = "white"});